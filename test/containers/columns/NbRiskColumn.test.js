import React from 'react'
import { shallow, mount, configure } from 'enzyme'
import renderer from 'react-test-renderer'
import { fromJS } from 'immutable'
import Adapter from 'enzyme-adapter-react-16'

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
} from 'material-ui/Table'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import { ONE_DAY } from '../../../src/constants/tenorData'
import NbRiskColumn from '../../../src/containers/columns/NbRiskColumn'
import App from '../../../src/app'
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

import { update } from '../../../src/actions/randomGenerator'
import {createStore} from 'redux'
import reducers from '../../../src/reducers'

describe('Shallow testing',() => {

  const initialState = { nbRisk: fromJS({INR: {'1D': 10, '2D': 20}}) }
  const mockStore = configureStore()
  let store,container

  beforeEach(() => {
    configure({ adapter: new Adapter() })
    store = mockStore(initialState)
  })

  it('nb risk column shallow', () => {
    container = shallow(<NbRiskColumn store={store} /> )
    expect(container.length).toEqual(1)
  })

  it('nb risk column mount', () => {
    const muiTheme = getMuiTheme({})
    container = mount(
      <Provider store={store}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <Table>
            <TableHeader>
              <TableRow>
                <TableHeaderColumn></TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
              <TableRow>
                <NbRiskColumn store={store}/>
              </TableRow>
            </TableBody>
          </Table>
        </MuiThemeProvider>
      </Provider> )
    expect(container.length).toEqual(1)
    expect(container.contains('1D')).toBe(true)
  })

  it('nb risk column snapshot should match', () => {
    const muiTheme = getMuiTheme({})
    const renderedValue =  renderer.create(
      <MuiThemeProvider muiTheme={muiTheme}>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn></TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox='false'>
            <TableRow>
              <NbRiskColumn store={store}/>
            </TableRow>
          </TableBody>
        </Table>
      </MuiThemeProvider>).toJSON()
    expect(renderedValue).toMatchSnapshot()
  })

})

