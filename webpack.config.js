const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractPlugin = new ExtractTextPlugin({
  filename: './style.css'
})

module.exports = {
  entry: "./index.js",
  output: {
    // CHANGED LINE
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  devtool: 'source-map',
  context: path.resolve(__dirname, 'src'),
  devServer: {
    contentBase: path.resolve(__dirname, 'public/assets'),
    stats: 'errors-only',
    open: true,
    port: 8080,
    compress: true
  },
  plugins: [
    new CleanWebpackPlugin(['public']),
    new HtmlWebpackPlugin({
      template: 'index.html'
    }),
    extractPlugin,
    // NEW LINES
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common'
    })
  ],
  // optimization: {
  //   splitChunks : {
  //     chunks: "all"
  //   }
  // },
  module: {
    rules: [{
      test: /\.(jpg|png|gif|svg)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './assets/',
          }
        }]
    }, {
      test: /\.css$/,
      use: extractPlugin.extract({
        use: ["css-loader"],
        fallback: 'style-loader'
      })
    }, {
      test: /\.js$/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env', 'stage-0', 'react']
        }
      }
    }]
  }
}