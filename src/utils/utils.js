import { TENORS } from '../constants/tenorData'
import { fromJS } from 'immutable'

export const initValue = val => {
  let symbolData = {}
  for (let tenor of TENORS)
    symbolData[tenor] = val

  return fromJS({
    INR: symbolData
  })
}
