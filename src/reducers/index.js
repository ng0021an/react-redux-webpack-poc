import { combineReducers } from "redux"
import randomNum from './randomReducer'
import nbRisk from './nbRiskReducer'
import live from './liveReducer'
import move from './moveReducer'
import livePL from './livePL'
import nbPL from './nbPLReducer'

export const reducers = combineReducers({
  randomNum,
  nbRisk,
  live,
  move,
  livePL,
  nbPL
})