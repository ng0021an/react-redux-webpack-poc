import { LIVE_PL } from '../constants/actiontypes/actionTypes'
import { initValue } from '../utils/utils'

const _intialState = initValue(0)

const livePL = (state = _intialState, action = {}) => {
  switch (action.type) {
    case LIVE_PL.UPDATE:
      return state.setIn([action.symbol, action.tenor], action.livePL)
    default:
      return state
  }
}

export default livePL