import { RANDOM_GENERATOR } from '../constants/actiontypes/actionTypes'
import { CLOSE } from '../constants/tenorData'
import { initValue } from '../utils/utils'

const _intialState = initValue(0)

const live = (state = _intialState, action = {}) => {
  switch (action.type) {
    case RANDOM_GENERATOR.UPDATE:
      return state.setIn([action.symbol, action.tenor], CLOSE[action.tenor] + action.randomNum / 10000)
    default:
      return state
  }
}

export default live