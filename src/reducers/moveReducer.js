import { MOVE } from '../constants/actiontypes/actionTypes'
import { initValue } from '../utils/utils'

const _intialState = initValue(0)

const move = (state = _intialState, action = {}) => {
  switch (action.type) {
    case MOVE.UPDATE:
      return state.setIn([action.symbol, action.tenor], action.move)
    default:
      return state
  }
}

export default move