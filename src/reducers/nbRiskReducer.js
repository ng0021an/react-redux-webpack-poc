import { RANDOM_GENERATOR } from '../constants/actiontypes/actionTypes'
import { RISK } from '../constants/tenorData'
import { initValue } from '../utils/utils'

const _intialState = initValue(0)

const nbRisk = (state = _intialState, action = {}) => {
  switch (action.type) {
    case RANDOM_GENERATOR.UPDATE:
      return state.setIn([action.symbol, action.tenor], 5 * RISK[action.tenor] * action.randomNum / 1000)
    default:
      return state
  }
}

export default nbRisk