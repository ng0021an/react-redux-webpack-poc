import { RANDOM_GENERATOR } from '../constants/actiontypes/actionTypes'
import { initValue } from '../utils/utils'

const _intialState = initValue(0)

const nbPL = (state = _intialState, action = {}) => {
  switch (action.type) {
    case RANDOM_GENERATOR.UPDATE:
      return state.setIn([action.symbol, action.tenor], action.randomNum * 25)
    default:
      return state
  }
}

export default nbPL