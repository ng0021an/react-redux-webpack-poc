import { MOVE } from '../constants/actiontypes/actionTypes'

export const update = (symbol, tenor, move) => ({
  type: MOVE.UPDATE,
  symbol,
  tenor,
  move,
})
