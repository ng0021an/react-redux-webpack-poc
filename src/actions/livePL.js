import { LIVE_PL } from '../constants/actiontypes/actionTypes'

export const update = (symbol, tenor, livePL) => ({
  type: LIVE_PL.UPDATE,
  symbol,
  tenor,
  livePL,
})
