import { RANDOM_GENERATOR } from '../constants/actiontypes/actionTypes'

export const update = (symbol, tenor, randomNum) => ({
  type: RANDOM_GENERATOR.UPDATE,
  symbol,
  tenor,
  randomNum,
})
