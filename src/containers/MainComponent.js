import React, { Component } from 'react'

import SymbolComponent from './SymbolComponent'
import styles from './styles'

export default class MainComponent extends Component {

  /**
   * this is easy for extension to include multiple currency or switching between currency, just need
   * to change the symbol property to some other value, re-rendering will auto kick in
   * due to time limit, leave that as enhancement
   */
  render = () => {
    return (
      <SymbolComponent symbol="INR"/>
    )
  }
}

