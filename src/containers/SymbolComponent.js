import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
} from 'material-ui/Table'

import {SORTED_TENOR_ENTRIES} from '../constants/tenorData'
import TenorColumn from './columns/TenorColumn'
import RandomColumn from './columns/RandomColumn'
import RiskColumn from './columns/RiskColumn'
import NbRiskColumn from './columns/NbRiskColumn'
import LiveRiskColumn from './columns/LiveRiskColumn'
import CloseColumn from './columns/CloseColumn'
import LiveColumn from './columns/LiveColumn'
import MoveColumn from './columns/MoveColumn'
import LivePLColumn from './columns/LivePLColumn'
import NbPLColumn from './columns/NbPLColumn'
import PLColumn from './columns/PLColumn'

import styles from './styles'

/**
 * Right now every random num change for any symbol/tenor will trigger whole column update
 * for all symbols due to the rigid nature of Redux store. Chose Redux due to time limit
 * A better way is to hand-wiring the components using Flux implemented with RxJs or listener
 */
export default class SymbolComponent extends Component {

  static propTypes = {
    symbol: PropTypes.string.isRequired,
  }

  render = () => {
    const {
      symbol
    } = this.props

    let rows = []
    for (let tenorEntry of SORTED_TENOR_ENTRIES) {
      rows.push(
        <TableRow key={`${symbol}_${tenorEntry[0]}`}>
          <TenorColumn    symbol={symbol} tenor={tenorEntry[0]} renderer='str'/>
          <RandomColumn   symbol={symbol} tenor={tenorEntry[0]} renderer='int'/>
          <RiskColumn     symbol={symbol} tenor={tenorEntry[0]} renderer='int'/>
          <NbRiskColumn   symbol={symbol} tenor={tenorEntry[0]} renderer='abs'/>
          <LiveRiskColumn symbol={symbol} tenor={tenorEntry[0]} renderer='abs'/>
          <CloseColumn    symbol={symbol} tenor={tenorEntry[0]} renderer='per'/>
          <LiveColumn     symbol={symbol} tenor={tenorEntry[0]} renderer='per'/>
          <MoveColumn     symbol={symbol} tenor={tenorEntry[0]} renderer='dec'/>
          <LivePLColumn   symbol={symbol} tenor={tenorEntry[0]} renderer='abs'/>
          <NbPLColumn     symbol={symbol} tenor={tenorEntry[0]} renderer='abs'/>
          <PLColumn       symbol={symbol} tenor={tenorEntry[0]} renderer='abs'/>
        </TableRow>
      )
    }

    rows.push(
      <TableRow key={`${symbol}_summary`}>
        <TenorColumn    symbol={symbol} summary='tot' renderer='str'/>
        <RandomColumn   symbol={symbol} summary='non' renderer='int'/>
        <RiskColumn     symbol={symbol} summary='sum' renderer='int'/>
        <NbRiskColumn   symbol={symbol} summary='sum' renderer='abs'/>
        <LiveRiskColumn symbol={symbol} summary='sum' renderer='abs'/>
        <CloseColumn    symbol={symbol} summary='ave' renderer='per'/>
        <LiveColumn     symbol={symbol} summary='ave' renderer='per'/>
        <MoveColumn     symbol={symbol} summary='ave' renderer='dec'/>
        <LivePLColumn   symbol={symbol} summary='sum' renderer='abs'/>
        <NbPLColumn     symbol={symbol} summary='sum' renderer='abs'/>
        <PLColumn       symbol={symbol} summary='sum' renderer='abs'/>
      </TableRow>
    )

    return (
      <div>
        <RaisedButton label={symbol} primary={true} />
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn></TableHeaderColumn>
              <TableHeaderColumn>Random</TableHeaderColumn>
              <TableHeaderColumn>Risk</TableHeaderColumn>
              <TableHeaderColumn>NB Risk</TableHeaderColumn>
              <TableHeaderColumn>Live Risk</TableHeaderColumn>
              <TableHeaderColumn>Close(%)</TableHeaderColumn>
              <TableHeaderColumn>Live(%)</TableHeaderColumn>
              <TableHeaderColumn>Move(BP)</TableHeaderColumn>
              <TableHeaderColumn>Live PL</TableHeaderColumn>
              <TableHeaderColumn>NB PL</TableHeaderColumn>
              <TableHeaderColumn>PL</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox='false'>
            {rows}
          </TableBody>
        </Table>
      </div>
    )
  }
}
