import AbstractColumn from './AbstractColumn'
import { CLOSE } from '../../constants/tenorData'

export default class CloseColumn extends AbstractColumn {

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    return CLOSE[tenor]
  }

}

