import ImmutablePropTypes from 'react-immutable-proptypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'
import * as randomGeneratorActionCreators from '../../actions/randomGenerator'
import randomFuntions from '../../constants/randomFunctions'

/**
 * Can pull some common code to an abstract column but not much so not doing for now
 */
class RandomColumn extends AbstractColumn {

  constructor(props) {
    super(props)
  }

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    randomNum: ImmutablePropTypes.map.isRequired
  })

  componentDidMount = () => {
    const {
      symbol,
      tenor,
      randomGeneratorActions
    } = this.props

    randomGeneratorActions.update(symbol, tenor, randomFuntions[symbol]())

    setInterval(() => {
      randomGeneratorActions.update(symbol, tenor, randomFuntions[symbol]())
    }, 5000)
  }

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      randomNum
    } = this.props

    return randomNum.getIn([symbol, tenor])
  }
}

const mapStateToPros = (state, ownProps) => ({
  randomNum: state.randomNum,
})

const mapDispatchToProps = dispatch => ({
  randomGeneratorActions: bindActionCreators(randomGeneratorActionCreators, dispatch),
})

export default connect(mapStateToPros, mapDispatchToProps)(RandomColumn)
