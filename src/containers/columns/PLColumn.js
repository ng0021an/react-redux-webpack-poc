import ImmutablePropTypes from 'react-immutable-proptypes'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'

class PLColumn extends AbstractColumn {

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    nbPL: ImmutablePropTypes.map.isRequired,
    livePL: ImmutablePropTypes.map.isRequired,
  })

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      nbPL,
      livePL
    } = this.props

    return nbPL.getIn([symbol, tenor]) + livePL.getIn([symbol, tenor])
  }

}

const mapStateToPros = (state, ownProps) => ({
  nbPL: state.nbPL,
  livePL: state.livePL,
})

export default connect(mapStateToPros, null)(PLColumn)
