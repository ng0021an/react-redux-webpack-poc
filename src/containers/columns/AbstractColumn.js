import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TableRowColumn } from 'material-ui/Table'

import CellRenderer from './CellRenderer'
import { TENORS } from '../../constants/tenorData'
import { sum, ave } from '../../utils/utils'

import styles from './styles'

export default class AbstractColumn extends Component {

  static propTypes = {
    symbol: PropTypes.string.isRequired,
    tenor: PropTypes.string,
    summary: PropTypes.string,
    renderer:PropTypes.string.isRequired
  }

  /**
   * to be overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {}

  _sum = symbol => {
    let res = 0
    for (let tenor of TENORS)
      res += this.getValueFor(symbol, tenor)
    return res
  }

  _ave = symbol => {
    return this._sum(symbol) / TENORS.length
  }

  render = () => {
    const {
      symbol,
      tenor,
      summary,
      renderer
    } = this.props

    let val = null

    //@TODO: rather than this if else switch, can also separate into separate summary component and pass in constructor

    if (summary) {
      switch (summary) {
        case 'sum': val = this._sum(symbol)
          break;
        case 'ave': val = this._ave(symbol)
          break;
        case 'tot': val = 'Total'
          break;
        default:
      }
    }
    else {
      val = this.getValueFor(symbol, tenor)
    }
    return renderer == 'str' ?
      (<TableRowColumn style={styles.tenorText}>{val ? val : ''}</TableRowColumn>)
      : (<CellRenderer type={renderer} val={val}/>)
  }
}