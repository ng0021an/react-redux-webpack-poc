import ImmutablePropTypes from 'react-immutable-proptypes'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'

class NbPLColumn extends AbstractColumn {

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    nbPL: ImmutablePropTypes.map.isRequired
  })

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      nbPL
    } = this.props

    return nbPL.getIn([symbol, tenor])
  }

}

const mapStateToPros = (state, ownProps) => ({
  nbPL: state.nbPL,
})

export default connect(mapStateToPros, null)(NbPLColumn)
