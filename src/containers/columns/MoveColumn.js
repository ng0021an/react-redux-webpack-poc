import ImmutablePropTypes from 'react-immutable-proptypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'
import * as moveActionCreators from '../../actions/move'
import { CLOSE } from '../../constants/tenorData'

class MoveColumn extends AbstractColumn {

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    live: ImmutablePropTypes.map.isRequired,
  })

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      live,
      moveActions
    } = this.props

    const val = (live.getIn([symbol, tenor]) - CLOSE[tenor]) * 10000

    //not the best thing to trigger event inside render, a better approach is to
    //separate calculation logic into separate class which will trigger calculation events,
    //that will require manual work and also tricky to get Redux store so leave this for now
    //Will be easier if we do hand-wiring of components

    moveActions.update(symbol, tenor, val)

    return val
  }

}

const mapStateToPros = (state, ownProps) => ({
  live: state.live,
})

const mapDispatchToProps = dispatch => ({
  moveActions: bindActionCreators(moveActionCreators, dispatch),
})

export default connect(mapStateToPros, mapDispatchToProps)(MoveColumn)
