import AbstractColumn from './AbstractColumn'

export default class TenorColumn extends AbstractColumn {

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    return tenor
  }

}

