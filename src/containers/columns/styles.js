export default {
  negText: {
    color: 'red'
  },
  posText: {
    color: 'green'
  },
  tenorText: {
    backgroundColor: 'grey',
    color: 'white'
  }
}