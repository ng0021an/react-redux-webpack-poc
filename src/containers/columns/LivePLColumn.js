import ImmutablePropTypes from 'react-immutable-proptypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'
import * as livePLActionCreators from '../../actions/livePL'
import { RISK } from '../../constants/tenorData'

class LivePLColumn extends AbstractColumn {

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    live: ImmutablePropTypes.map.isRequired,
  })

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      move,
      livePLActions
    } = this.props

    const val = move.getIn([symbol, tenor]) * RISK[tenor]

    //not the best thing to trigger event inside render, a better approach is to
    //separate calculation logic into separate class which will trigger calculation events,
    //that will require manual work and also tricky to get Redux store so leave this for now
    //Will be easier if we do hand-wiring of components

    livePLActions.update(symbol, tenor, val)

    return val
  }

}

const mapStateToPros = (state, ownProps) => ({
  move: state.move,
})

const mapDispatchToProps = dispatch => ({
  livePLActions: bindActionCreators(livePLActionCreators, dispatch),
})

export default connect(mapStateToPros, mapDispatchToProps)(LivePLColumn)
