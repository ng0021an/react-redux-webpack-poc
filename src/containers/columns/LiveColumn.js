import ImmutablePropTypes from 'react-immutable-proptypes'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'

class LiveColumn extends AbstractColumn {

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    live: ImmutablePropTypes.map.isRequired
  })

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      live
    } = this.props

    return live.getIn([symbol, tenor])
  }

}

const mapStateToPros = (state, ownProps) => ({
  live: state.live,
})

export default connect(mapStateToPros, null)(LiveColumn)
