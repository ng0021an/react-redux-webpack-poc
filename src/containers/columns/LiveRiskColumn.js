import ImmutablePropTypes from 'react-immutable-proptypes'
import { connect } from 'react-redux'
import _ from 'lodash'

import AbstractColumn from './AbstractColumn'
import { RISK } from '../../constants/tenorData'

class LiveRiskColumn extends AbstractColumn {

  static propTypes = _.assign({}, AbstractColumn.propTypes, {
    nbRisk: ImmutablePropTypes.map.isRequired,
  })

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    const {
      nbRisk,
    } = this.props

    return nbRisk.getIn([symbol, tenor]) + RISK[tenor]
  }

}

const mapStateToPros = (state, ownProps) => ({
  nbRisk: state.nbRisk,
})

export default connect(mapStateToPros, null)(LiveRiskColumn)
