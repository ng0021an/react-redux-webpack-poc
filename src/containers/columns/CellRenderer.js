import React, { Component } from 'react'
import PropTypes from 'prop-types'
import numeral from 'numeral'
import { TableRowColumn } from 'material-ui/Table'

import styles from './styles'

export default class CellRenderer extends Component {

  static propTypes = {
    type: PropTypes.string.isRequired,
    val: PropTypes.number,
  }

  _int = val => {
    return this._render(val, numeral(val).format('0'))
  }

  _intAbs = val => {
    if (val == null)
      return (<TableRowColumn/>)
    const temp = numeral(Math.abs(val)).format('0,0')
    return this._render(val, val < 0 ? `(${temp})` : `${temp}`)
  }

  _percent = val => {
    return this._render(val, numeral(val).format('0.00%'))
  }

  _decimal = val => {
    return this._render(val, numeral(val).format('0.00'))
  }

  _default = val => {
    if (val == null)
      return (<TableRowColumn/>)
    return this._render(val, `${val}`)
  }

  _render = (val, text) => {
    if (val == null)
      return (<TableRowColumn/>)

    return val < 0 ? (<TableRowColumn style={styles.negText}>{text}</TableRowColumn>)
      : (<TableRowColumn style={styles.posText}>{text}</TableRowColumn>)
  }

  render = () => {
    const {
      type,
      val,
    } = this.props

    let toRender = ''

    //@TODO: again, rather than this if else can move to separate components and pass in constructor

    switch (type) {
      case 'str': toRender = val
        break;
      case 'int': toRender = this._int(val)
        break;
      case 'abs': toRender = this._intAbs(val)
        break;
      case 'per': toRender = this._percent(val)
        break;
      case 'dec': toRender = this._decimal(val)
        break;
      default: toRender = this._default(val)
    }

    return toRender
  }
}