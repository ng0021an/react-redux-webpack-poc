import AbstractColumn from './AbstractColumn'
import { RISK } from '../../constants/tenorData'

export default class RiskColumn extends AbstractColumn {

  /**
   * Overridden
   * @param symbol
   * @param tenor
   */
  getValueFor = (symbol, tenor) => {
    return RISK[tenor]
  }

}

