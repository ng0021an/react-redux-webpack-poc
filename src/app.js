import React from "react";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { Provider } from 'react-redux'
import { store } from './store.js'

import MainComponent from "./containers/MainComponent"

const muiTheme = getMuiTheme({})

const App = props => <Provider store={store}>
  <MuiThemeProvider muiTheme={muiTheme}>
    <MainComponent/>
  </MuiThemeProvider>
</Provider>

export default App