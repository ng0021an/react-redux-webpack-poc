import createRequestTypes from './actionTypesUtil'

export const RANDOM_GENERATOR = createRequestTypes('RANDOM_GENERATOR')
export const MOVE = createRequestTypes('MOVE')
export const LIVE_PL = createRequestTypes('LIVE_PL')
