const UPDATE = 'UPDATE'

const createRequestTypes = (base) => (
  [
    UPDATE
  ].reduce((acc, type) => {
    acc[type] = `${base}/${type}`
    return acc
  }, {})
)

export default createRequestTypes
