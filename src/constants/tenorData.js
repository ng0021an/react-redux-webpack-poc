import _ from 'lodash'

/**
 * maps inside this file can be changed to include symbol when we need to introduce more symbols
 * {symbol => {tenor => data}}
 * Keep like this for simplicity for now
 */

export const ONE_DAY = '1D'
export const ONE_WEEK = '1W'
export const ONE_MONTH = '1M'
export const THREE_MONTH = '3M'
export const SIX_MONTH = '6M'
export const ONE_YEAR = '1Y'
export const TWO_YEAR = '2Y'
export const FIVE_YEAR = '5Y'
export const TEN_YEAR = '10Y'
export const TWENTY_YEAR = '20Y'

export const TENORS = [
  ONE_DAY,
  ONE_WEEK,
  ONE_MONTH,
  THREE_MONTH,
  SIX_MONTH,
  ONE_YEAR,
  TWO_YEAR,
  FIVE_YEAR,
  TEN_YEAR,
  TWENTY_YEAR
]

export const RISK = {}
RISK[ONE_DAY] = 1000
RISK[ONE_WEEK] = 1200
RISK[ONE_MONTH] = 1400
RISK[THREE_MONTH] = 1600
RISK[SIX_MONTH] = 1800
RISK[ONE_YEAR] = -1600
RISK[TWO_YEAR] = -1400
RISK[FIVE_YEAR] = 1600
RISK[TEN_YEAR] = 1800
RISK[TWENTY_YEAR] = 2000

export const CLOSE = {}
CLOSE[ONE_DAY] = 0.05
CLOSE[ONE_WEEK] = 0.053
CLOSE[ONE_MONTH] = 0.056
CLOSE[THREE_MONTH] = 0.059
CLOSE[SIX_MONTH] = 0.062
CLOSE[ONE_YEAR] = 0.065
CLOSE[TWO_YEAR] = 0.068
CLOSE[FIVE_YEAR] = 0.071
CLOSE[TEN_YEAR] = 0.074
CLOSE[TWENTY_YEAR] = 0.077

export const SORT_KEYS = {}
SORT_KEYS[ONE_DAY] = 1
SORT_KEYS[ONE_WEEK] = 7
SORT_KEYS[ONE_MONTH] = 30
SORT_KEYS[THREE_MONTH] = 90
SORT_KEYS[SIX_MONTH] = 180
SORT_KEYS[ONE_YEAR] = 365
SORT_KEYS[TWO_YEAR] = 730
SORT_KEYS[FIVE_YEAR] = 1825
SORT_KEYS[TEN_YEAR] = 3650
SORT_KEYS[TWENTY_YEAR] = 7300

export const SORTED_TENOR_ENTRIES = _.sortBy(Object.entries(SORT_KEYS), entry => entry[1])

